package myshout.corsair.com.shout;

/**
 * Created by Tiash on 2/15/2016.
 */

import java.io.IOException;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class AlarmReceiverActivity extends Activity
{

    //private MediaPlayer mMediaPlayer;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        /// int s= Math.
        SharedPreferences smsSet = getSharedPreferences("smsCondition",Context.MODE_PRIVATE);
	  		/*boolean state = smsSet.getBoolean("trigger", false);
	  		if (state == true)
	  		{*/
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(smsSet.getString("number", "+880"), null, smsSet.getString("message", "This is an Automated SMS"), null, null);
	  		/*}

	  		else
	  		{
	  			Toast.makeText(getApplicationContext(), "Message was cancelled", 10).show();
	  		}


*/
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

        // Build notification
        // Actions are just fake
        Notification noti = new Notification.Builder(this)
                .setContentTitle("Timer Message Sent")
                .setContentText("Press here to have more security options").setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pIntent).build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, noti);
        // finish();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));

    }


}
