package myshout.corsair.com.shout;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import myshout.corsair.com.shout.SimpleGestureFilter.SimpleGestureListener;
import org.w3c.dom.Text;

/**
 * Created by Tiash on 3/9/2016.
 */
public class AllInfo extends AppCompatActivity implements SimpleGestureListener {
    public static String [] prgmImages=
            {
            "0000000001",
            "0000000002",
            "0000000003",
            "0000000004",
            "0000000005",
            "0000000006",
            "0000000007",
            "0000000008",
            };

    private SimpleGestureFilter detector;
    public static String [] prgmNameList={
            "Fahima Akter",
            "Rasheda Akter",
            "Sayeed Hasan",
            "Afroja Banu",
            "Razia Khatun",
            "Amir Ali",
            "Rokeya Begum",
            "Nujhat Rahman"
    };
    int state = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allinfo);


        // animation thing
        ImageView animation = (ImageView) findViewById(R.id.sheep);
        animation.setBackgroundResource(R.drawable.animation);
        AnimationDrawable frameAnimation = (AnimationDrawable) animation.getBackground();
        frameAnimation.start();

        SharedPreferences settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
        String zilla = settings.getString("zilla", "Dhaka");
        String namee = settings.getString("name","hii");
        TextView tv1 = (TextView) findViewById(R.id.name);
        tv1.setText(namee);
        TextView tv2= (TextView) findViewById(R.id.zil);
        tv2.setText(zilla);

        // Detect touched area
        detector = new SimpleGestureFilter(this,this);

        // icon resize
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        width = (width* 95)/100;

        CardView card = (CardView) findViewById(R.id.op1);
        card.getLayoutParams().height=(height/8);
        card.getLayoutParams().width=width/4;

        CardView card3 = (CardView) findViewById(R.id.op3);
        card3.getLayoutParams().height=(height/8);
        card3.getLayoutParams().width=width/4;

        CardView card2 = (CardView) findViewById(R.id.op2);
        card2.getLayoutParams().height=(height/8);
        card2.getLayoutParams().width=width/2;

        ImageView im1 = (ImageView) findViewById(R.id.top);
        //im1.getLayoutParams().width= displayMetrics.widthPixels;
       // Law();
       View myv = findViewById(R.id.lay1);
        Law(myv);


    }



    public void OCC(View v)
    {
        state = 3;
        ImageView top = (ImageView) findViewById(R.id.top);
        top.setImageResource(R.drawable.chat2);

        LinearLayout lay2 = (LinearLayout) findViewById(R.id.lay2);
        lay2.setVisibility(v.GONE);
        LinearLayout lay3 = (LinearLayout) findViewById(R.id.lay3);
        lay3.setVisibility(v.GONE);
        LinearLayout lay1 = (LinearLayout) findViewById(R.id.lay1);
        lay1.setVisibility(v.VISIBLE);

        SharedPreferences settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
        String zilla = settings.getString("zilla","Dhaka");
        Occ myOcc = new Occ(zilla);
        OccAll tias = myOcc.getAddress();
        TextView tv1 =(TextView) findViewById(R.id.tv1);
        tv1.setText(tias.name);
        YoYo.with(Techniques.Wave)
                .duration(400)
                .playOn(findViewById(R.id.op3));

    }
    public void Law(View v)
    {
        state =1;
        ImageView top = (ImageView) findViewById(R.id.top);
        top.setImageResource(R.drawable.chat1);
        LinearLayout lay1 = (LinearLayout) findViewById(R.id.lay1);
        lay1.setVisibility(v.GONE);
        LinearLayout lay3 = (LinearLayout) findViewById(R.id.lay3);
        lay3.setVisibility(v.GONE);

        LinearLayout lay2 = (LinearLayout) findViewById(R.id.lay2);
        lay2.setVisibility(v.VISIBLE);
        TextView mtv1 = (TextView) findViewById(R.id.mtv1);
        TextView mtv4 = (TextView) findViewById(R.id.mtv4);
        TextView mtv5 = (TextView) findViewById(R.id.mtv5);

        SharedPreferences settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
        String zilla = settings.getString("zilla", "Dhaka");
        LawHelp law = new LawHelp(zilla);
        int pos = law.nearLaw(zilla);
        YoYo.with(Techniques.Wave)
                .duration(400)
                .playOn(findViewById(R.id.op1));
        mtv1.setText(law.office[pos]);
        mtv4.setText(law.add1[pos]);
        mtv5.setText(law.add2[pos]);


        /*
        ListView lv=(ListView) findViewById(R.id.listView);
        lv.setAdapter(new CustomAdapter(getApplicationContext(), prgmNameList, prgmImages));*/

    }

    public void ChangeMakers(View v)
    {
        state =2;
        ImageView img = (ImageView) findViewById(R.id.top);
        img.setImageResource(R.drawable.chat3);
        LinearLayout lay2 = (LinearLayout) findViewById(R.id.lay2);
        lay2.setVisibility(v.GONE);
        LinearLayout lay1 = (LinearLayout) findViewById(R.id.lay1);
        lay1.setVisibility(v.GONE);
        LinearLayout lay3 = (LinearLayout) findViewById(R.id.lay3);
        lay3.setVisibility(v.VISIBLE);
        YoYo.with(Techniques.Wave)
                .duration(400)
                .playOn(findViewById(R.id.op2));



        ListView lv=(ListView) findViewById(R.id.listView);
        lv.setAdapter(new CustomAdapter(getApplicationContext(), prgmNameList, prgmImages));
    }

    public void KnowLaw(View v)
    {
        YoYo.with(Techniques.Shake)
                .duration(400)
                .playOn(findViewById(R.id.lawcard));
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }
    public void Right()
    {
        if (state ==1){
           // Toast.makeText(this, "change maker", Toast.LENGTH_SHORT).show();
            ChangeMakers(findViewById(R.id.lay2));
        }
        else if (state ==2){
            OCC(findViewById(R.id.lay3));
        }
        else if (state ==3){
            Law(findViewById(R.id.lay1));
        }
    }
    public void Left()
    {
        if (state ==3){
            //Toast.makeText(this, "change maker", Toast.LENGTH_SHORT).show();
            ChangeMakers(findViewById(R.id.lay2));
        }
        else if (state ==2){
            Law(findViewById(R.id.lay1));
        }
        else if (state ==1){
            OCC(findViewById(R.id.lay3));
        }
    }
    @Override
    public void onSwipe(int direction) {
        String str = "";

        switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT : str = "Swipe Right"; Right();
                break;
            case SimpleGestureFilter.SWIPE_LEFT :  str = "Swipe Left"; Left();
                break;
            case SimpleGestureFilter.SWIPE_DOWN :  str = "Swipe Down";
                break;
            case SimpleGestureFilter.SWIPE_UP :    str = "Swipe Up";
                break;

        }
     //   Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDoubleTap()
    {

    }
}
