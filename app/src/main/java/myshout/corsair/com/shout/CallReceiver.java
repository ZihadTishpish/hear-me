package myshout.corsair.com.shout;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.widget.Toast;

/**
 * Created by Tiash on 3/4/2016.
 */
public class CallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String phoneState = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

        if(phoneState.equals(TelephonyManager.EXTRA_STATE_RINGING))
        {
            Toast.makeText(context,"call in progress",Toast.LENGTH_SHORT).show();
            context.stopService(new Intent(context,ChatHead.class));
        }
    }

}


/*
<receiver android:name=".CallReceiver" android:enabled="true">
            <intent-filter>
                <action android:name="android.intent.action.PHONE_STATE" />
            </intent-filter>
        </receiver>
 */