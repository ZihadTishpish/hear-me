package myshout.corsair.com.shout;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;

import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import myshout.corsair.com.shout.SimpleGestureFilter.SimpleGestureListener;

/**
 * Created by Akash on 3/18/2016.
 */
public class FakeCall2 extends Activity implements SimpleGestureListener {


    private SimpleGestureFilter detector;
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fake_ringing);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        mp = MediaPlayer.create(getApplicationContext(), notification);
        mp.start();

        // Detect touched area
        detector = new SimpleGestureFilter(this,this);

    }
    public void Backk(View v)
    {

        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }
    public void Right()
    {
        //startActivity(new Intent(getApplicationContext(), FakeCall3.class));
        mp.stop();
        setContentView(R.layout.activity_fake_ongoing);
    }
    public void Left()
    {
        mp.stop();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }
    @Override
    public void onSwipe(int direction) {
        String str = "";
     switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT:
                str = "Swipe Right";
                Right();
                break;
            case SimpleGestureFilter.SWIPE_LEFT:
                str = "Swipe Left";
                Left();
                break;
            case SimpleGestureFilter.SWIPE_DOWN:
                str = "Swipe Down";
                break;
            case SimpleGestureFilter.SWIPE_UP:
                str = "Swipe Up";
                break;

        }
    }

    @Override
    public void onDoubleTap()
    {

    }
}
