package myshout.corsair.com.shout;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.widget.LinearLayout;

/**
 * Created by Tiash on 2/26/2016.
 */
public class InfoClass extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infoui);
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        height = (height* 95)/100;

        ///////////////////////////left side
        CardView card = (CardView) findViewById(R.id.card);
        card.getLayoutParams().height=(height*5)/10;
        card.getLayoutParams().width=width/2;

        CardView card2 = (CardView) findViewById(R.id.card2);
        card2.getLayoutParams().height=(height*2/10);
        card2.getLayoutParams().width=width/2;


        CardView card5 = (CardView) findViewById(R.id.card5);
        card5.getLayoutParams().height=(height*3)/10;
        card5.getLayoutParams().width=width/2;

        //////////////////////////////////////////////right side

        CardView card3 = (CardView) findViewById(R.id.card3);
        card3.getLayoutParams().height=height/4;
        card3.getLayoutParams().width=width/2;

        CardView card4 = (CardView) findViewById(R.id.card4);
        card4.getLayoutParams().height=height/6;
        card4.getLayoutParams().width=width/2;

        /*
        CardView card6 = (CardView) findViewById(R.id.card6);
        card6.getLayoutParams().height=height/3;
        card6.getLayoutParams().width=width/2;
        */

    }
}
