package myshout.corsair.com.shout;

/**
 * Created by Tiash on 3/10/2016.
 */
public class LawHelp
{
    String zilla;
    int pos;
    public String office[] = {
            "কেন্দ্রীয় অফিস",
            "রাজশাহী বিভাগীয় অফিস",
            "চট্রগ্রাম বিভাগীয় অফিস",
            "খুলনা বিভাগীয় অফিস",
            "সিলেট বিভাগীয় অফিস"
    };
    public String add1[] = {
            "মনিকো টাওয়ার, ৪৮/৩",
    "মহলদার পাড়া ,শিরোইল",
    "জেলা পরিষদ ভবন(টিন শেড বিল্ডিং, ২য় তলা)",
    "সেলিনা হাউজ, হোল্ডিং নং নতুন-১,পুরাতন-১১২",
    "বাড়ী নং- ৪৪,রোড নং- ২ , ব্লক নং- ই"
    };
    public String add2[] = {
            "পশ্চিম আগারগাও, ঢাকা-১২০৭",
            "রাজশাহী- ৬১০০",
            "কোর্ট রোড-চট্টগ্রাম",
            "(৩য় তলা) ফকিরবাড়ী লেন, হাজী মহসীন রোড,খুলনা",
            "শাহাজালাল উপশহর , সিলেট"
    };
    LawHelp(String s)
    {
        zilla = s;
    }
    int nearLaw(String s)
    {
        if (s.matches("Dhaka"))
        {
            return 0;
        }
        else if (s.matches("Rajshahi"))
        {
            return 1;
        }
        else if (s.matches("Chittagong"))
        {
            return 2;
        }
        else if (s.matches("Khulna"))
        {
            return 3;
        }
        else
        {
            return 4;
        }
    }
}
