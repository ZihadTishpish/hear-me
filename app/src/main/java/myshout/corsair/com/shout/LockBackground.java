package myshout.corsair.com.shout;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.widget.Toast;

import java.util.ArrayList;

import android.content.SharedPreferences;import android.telephony.gsm.SmsManager;


/**
 * Created by Tiash on 2/12/2016.
 */
public class LockBackground extends Activity {
    private long mLastClickTime = 0;
   // private LockBackground lockBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();

        if(bundle != null)
        {

            if (bundle.getBoolean("close")== true)
            {
                //lockBackground.finishActivity(0);
                Toast.makeText(getApplicationContext(), "This should be closed", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
        else {

            setContentView(R.layout.lockback);
            if (isMyServiceRunning(ChatHead.class) == false) {
                Toast.makeText(getApplicationContext(), "not running", Toast.LENGTH_SHORT).show();
                startService(new Intent(getApplicationContext(), ChatHead.class));
            } else {
                Toast.makeText(getApplicationContext(), "Already runing running", Toast.LENGTH_SHORT).show();
                // stopService(new Intent(getApplicationContext(),ChatHead.class));
            }
        }
       // startService(new Intent(getApplicationContext(), ChatHead.class));

    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_POWER))
        {
            startActivity(new Intent(getApplicationContext(),playAlarm.class));
        }

        else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN )  //INSTANT SMS
        {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 800)
            {
                Toast.makeText(getApplicationContext(),"Detected", Toast.LENGTH_LONG).show();
            /////////////////////////////////////

                SharedPreferences settings = getSharedPreferences("settings",Context.MODE_PRIVATE);
                String namee = settings.getString("name", "null");
                String gett = settings.getString("blabla", "null");
                String okk = settings.getString("number", "+880");
                if (gett.equals("null"))
                {
                    gett = "Help me. I am in Danger.";

                }
                Toast.makeText(getApplicationContext(),"Sending SMS...", Toast.LENGTH_LONG).show();

                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(okk, null, gett, null, null);
                ////////////////////////////////////////////
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            // Toast.makeText(getApplicationContext(),"Tap Again to Exit", Toast.LENGTH_LONG).show();
        }

        else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP )
        {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 800)
            {
                Toast.makeText(getApplicationContext(),"Detected too", Toast.LENGTH_LONG).show();
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            // Toast.makeText(getApplicationContext(),"Tap Again to Exit", Toast.LENGTH_LONG).show();
        }
        if (keyCode == KeyEvent.KEYCODE_BACK )
        {
             finish();
        }

        return true;
    }

}

