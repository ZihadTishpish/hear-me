package myshout.corsair.com.shout;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    int width=0;
    View view;
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ////////////View Design Start /////////////////////////////////////////

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        height = (height* 95)/100;
        playAnimation();
        ///////////////////////////left side
        CardView card = (CardView) findViewById(R.id.card);
        card.getLayoutParams().height=(height/4);
        card.getLayoutParams().width=width/2;

        CardView card2 = (CardView) findViewById(R.id.card2);
        card2.getLayoutParams().height=(height/4);
        card2.getLayoutParams().width=width/2;

        CardView card5 = (CardView) findViewById(R.id.card5);
        card5.getLayoutParams().height=(height/4);
        card5.getLayoutParams().width=width/2;

        //////////////////////////////////////////////right side

        CardView card3 = (CardView) findViewById(R.id.card3);
        card3.getLayoutParams().height=height/4;
        card3.getLayoutParams().width=width/2;

        CardView card4 = (CardView) findViewById(R.id.card4);
        card4.getLayoutParams().height=height/4;
        card4.getLayoutParams().width=width/2;


        CardView card6 = (CardView) findViewById(R.id.card6);
        card6.getLayoutParams().height=height/4;
        card6.getLayoutParams().width=width/2;

        ///////////// View design end /////////////////////////




        SharedPreferences settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
        boolean existance = settings.getBoolean("existance", false);
        String lockState = settings.getString("available","no");
        if (lockState.equals("yes"))
        {
            if (isMyServiceRunning(UpdateService.class)== false) {
                stopService(new Intent(getApplicationContext(),UpdateService.class));
                startService(new Intent(getApplicationContext(), UpdateService.class));
            }
            else
            {
                stopService(new Intent(getApplicationContext(),UpdateService.class));
                startService(new Intent(getApplicationContext(), UpdateService.class));
            }
        }

        if (existance == false   )
        {
            TakeInfo();
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {

            super.onBackPressed();

    }
    public void TakeInfo()   // this is where sharedpreference save data
    {
        final WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.maindlog, null);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                width-200,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE,
                PixelFormat.TRANSLUCENT);

        android.support.v7.widget.AppCompatSpinner spinner = (android.support.v7.widget.AppCompatSpinner) view.findViewById(R.id.zilla);
        List<String> bloodlist = new ArrayList<String>();
        bloodlist.add("Dhaka");
        bloodlist.add("Rajshahi");
        bloodlist.add("Chittagong");
        bloodlist.add("Khulna");
        bloodlist.add("Sylhet");
        ArrayAdapter<String> dataAdapter= new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,bloodlist);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinner.setAdapter(dataAdapter);
        YoYo.with(Techniques.SlideInRight)
                .duration(500)
                .playOn(view.findViewById(R.id.rel1));
       ImageView yes = (ImageView) view.findViewById(R.id.yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vie) {

                YoYo.with(Techniques.Wave)
                        .duration(200)
                        .interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation) {


                            }

                            @Override
                            public void onAnimationEnd(Animator animation)
                            {EditText e1 = (EditText) view.findViewById(R.id.e1);
                                EditText e2 = (EditText) view.findViewById(R.id.e2);
                                SharedPreferences settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
                                //boolean existance = settings.getBoolean("existance", false);
                                SharedPreferences.Editor edit = settings.edit();
                                if (e1.getText().toString().length()> 1 && e2.getText().toString().length()> 6 ) {

                                    edit.putBoolean("existance", true);

                                    edit.putString("name", e1.getText().toString());

                                    edit.putString("number", e2.getText().toString());

                                    AppCompatSpinner sp = (AppCompatSpinner) view.findViewById(R.id.zilla);
                                    edit.putString("zilla", sp.getSelectedItem().toString());
                                    Toast.makeText(getApplicationContext(), "You can Always Change info from preferences", Toast.LENGTH_SHORT).show();
                                    edit.commit();

                                    YoYo.with(Techniques.FlipOutX)
                                            .duration(500)
                                            .interpolate(new AccelerateDecelerateInterpolator())
                                            .withListener(new Animator.AnimatorListener() {

                                                @Override
                                                public void onAnimationStart(Animator animation) {

                                                }

                                                @Override
                                                public void onAnimationEnd(Animator animation) {
                                                    windowManager.removeViewImmediate(view);
                                                }

                                                @Override
                                                public void onAnimationCancel(Animator animation) {
                                                }

                                                @Override
                                                public void onAnimationRepeat(Animator animation) {
                                                }
                                            }).playOn(view.findViewById(R.id.rel1));
                                    //windowManager.removeViewImmediate(view);
                                }
                                else
                                {
                                    Toast.makeText(getApplicationContext(), "Please Provide valid name and mobile number", Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {
                            }
                        }).playOn(view.findViewById(R.id.yes));
            }
        });

        ImageView noo = (ImageView) view.findViewById(R.id.noo);
        noo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vie) {

                YoYo.with(Techniques.ZoomOut)
                        .duration(1100)
                        .playOn(view.findViewById(R.id.card));
                YoYo.with(Techniques.FlipOutX)
                        .duration(500)
                        .interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                windowManager.removeViewImmediate(view);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {
                            }
                        }).playOn(view.findViewById(R.id.rel1));
            }
        });
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.x = 0;
        params.y = 100;
        windowManager.addView(view, params);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void StartSiren(View v)
    {
        startActivity( new Intent(getApplicationContext(),playAlarm.class));
    }

    public void Record(View v)
    {

        YoYo.with(Techniques.Wave)
                .duration(200)
                .interpolate(new AccelerateDecelerateInterpolator())
                .withListener(new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                        // View v = R.id.image6;
                        startActivity( new Intent(getApplicationContext(),Record.class));
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {}

                    @Override
                    public void onAnimationRepeat(Animator animation) {}
                }).playOn(findViewById(R.id.card5));
    }
    public void SpyCam(View v)
    {

        YoYo.with(Techniques.Wave)
                .duration(200)
                .interpolate(new AccelerateDecelerateInterpolator())
                .withListener(new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                        // View v = R.id.image6;
                        startActivity( new Intent(getApplicationContext(),Spycamera.class));
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {}

                    @Override
                    public void onAnimationRepeat(Animator animation) {}
                }).playOn(findViewById(R.id.card2));

    }
    public void FakeCallSelect(View v)
    {

        YoYo.with(Techniques.Wave)
                .duration(200)
                .interpolate(new AccelerateDecelerateInterpolator())
                .withListener(new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                        startActivity(new Intent(getApplicationContext(), FakeCall2.class));
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {}

                    @Override
                    public void onAnimationRepeat(Animator animation) {}
                }).playOn(findViewById(R.id.card6));
    }
    public void TimerSMS(View v)
    {

        YoYo.with(Techniques.Wave)
                .duration(200)
                .interpolate(new AccelerateDecelerateInterpolator())
                .withListener(new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                        startActivity(new Intent(getApplicationContext(), setTimer.class));
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {}

                    @Override
                    public void onAnimationRepeat(Animator animation) {}
                }).playOn(findViewById(R.id.card4));
    }
    public void Police(View v)
    {
        YoYo.with(Techniques.Wave)
                .duration(200)
                .interpolate(new AccelerateDecelerateInterpolator())
                .withListener(new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                        SharedPreferences settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
                        boolean existance = settings.getBoolean("existance", false);
                        if (existance == false)
                        {
                            Toast.makeText(getApplicationContext(),"Please provide info and try again",Toast.LENGTH_LONG).show();
                            TakeInfo();
                        }
                        else
                        startActivity( new Intent(getApplicationContext(),AllInfo.class));
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {}

                    @Override
                    public void onAnimationRepeat(Animator animation) {}
                }).playOn(findViewById(R.id.card));
    }

    public void MainPolice(View v)
    {

        startActivity(new Intent(getApplicationContext(), PoliceMain.class));
    }



    public void lockScreen(View v)
    {

        YoYo.with(Techniques.Wave)
                .duration(200)
                .interpolate(new AccelerateDecelerateInterpolator())
                .withListener(new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                        SharedPreferences settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
                        String state = settings.getString("available","no");
                        if (state.equals("no")) {
                            startActivity(new Intent(getApplicationContext(), LockScreenSetup.class));
                        }
                        else {
                            startActivity(new Intent(getApplicationContext(), LockChangeConfirm.class));
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {}

                    @Override
                    public void onAnimationRepeat(Animator animation) {}
                }).playOn(findViewById(R.id.card3));

    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera)
        {

            //startActivity(new Intent (getApplicationContext(),mainUI.class));
        } else if (id == R.id.nav_gallery) {

           // startActivity(new Intent (getApplicationContext(), InfoClass.class));

        }  else if (id == R.id.nav_manage) {

            TakeInfo();
            /////Change preferences , edit emergency number dlog box

        }else if (id == R.id.nav_slideshow) {
            // startActivity(new Intent (getApplicationContext(),hotash.class));

        } else if (id == R.id.nav_share) {
            startActivity(new Intent (getApplicationContext(),hotash.class));

        } else if (id == R.id.nav_send) {
            startActivity(new Intent (getApplicationContext(),hotash.class));

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public  void playAnimation()
    {

        YoYo.with(Techniques.SlideInDown)
                .duration(1100)
                .playOn(findViewById(R.id.card));

        YoYo.with(Techniques.SlideInLeft)
                .duration(1100)
                .playOn(findViewById(R.id.card2));

        YoYo.with(Techniques.SlideInUp)
                .duration(1100)
                .playOn(findViewById(R.id.card5));

        YoYo.with(Techniques.SlideInDown)
                .duration(1100)
                .playOn(findViewById(R.id.card3));

        YoYo.with(Techniques.SlideInRight)
                .duration(1100)
                .playOn(findViewById(R.id.card4));

        YoYo.with(Techniques.SlideInUp)
                .duration(1100)
                .playOn(findViewById(R.id.card6));

    }
}
