package myshout.corsair.com.shout;

import android.app.Activity;
import android.os.Bundle;

import java.util.ArrayList;

/**
 * Created by Tiash on 3/4/2016.
 */
public class MyRootActivity extends Activity
{
    private static final String TAG=MyRootActivity.class.getName();
    private static ArrayList<Activity> activities=new ArrayList<Activity>();


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        activities.add(this);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        activities.remove(this);
    }
    public static Activity getActivity()
    {
        return activities.get(0);
    }
    public static void finishAll()
    {
        for(Activity activity:activities)
            activity.finish();
    }
}
