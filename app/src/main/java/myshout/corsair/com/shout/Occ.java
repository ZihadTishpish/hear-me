package myshout.corsair.com.shout;

/**
 * Created by Tiash on 3/10/2016.
 */
public class Occ
{
    String zilla;
    OccAll tias;
    public static String [] mob={
            "01713423490",
            "01713366637",
            "01713366635",
            "01713366634",
            "01713366634"};

    public static String [] Name={
            "Dhaka Medical College",
            "Rajshahi Medical College",
            "Chittagong Medical College",
            "MAG Osmani Medical College",
            "Khulna Medical College"};
    public String [] pos1={
            "23.725214","24.372343","22.360168","24.900402","22.829144"};
    public String [] pos2={
            "90.397500","88.585679","91.831670","91.853192","89.537115"};

    Occ(String s)
    {
        zilla = s;
    }

    OccAll getAddress()
    {
        int pos = nearOCC(zilla);
        OccAll oc = new OccAll(Name[pos],mob[pos],pos1[pos],pos2[pos]);
        return oc;
    }

    int nearOCC(String s)
    {
        if (s.matches("Dhaka"))
        {
            return 0;
        }
        else if (s.matches("Rajshahi"))
        {
        return 1;
        }
        else if (s.matches("Chittagong"))
        {
            return 2;
        }
        else if (s.matches("Sylhet"))
        {
            return 3;
        }
        else
        {
            return 4;
        }
    }
}
