package myshout.corsair.com.shout;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Tiash on 2/21/2016.
 */
public class Police extends Activity {
    ListView lv;
    Context context;
    ArrayList prgmName;

    public static String [] prgmImages={"01713423490","01713366637","01713366635","01713366634", "01713366634", "01713366638","01755584573","01755584574", "01713177175"};

    public static String [] prgmNameList={"Dhaka Medical College","Rajshahi Medical College", "Chittagong Medical College", "MAG Osmani Medical College"
            , "Khulna Medical College", "Sher-E-Bangla Medical College", "Faridpur Medical College","Rangpur Medical College",
            "National Trauma Counseling Centre"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.police);
        context=this;

        lv=(ListView) findViewById(R.id.listView);
        lv.setAdapter(new CustomAdapter(this, prgmNameList,prgmImages));

    }
    public void OCCHelp(View v)
    {

       Intent intent = new Intent(getApplicationContext(), OCCHelp.class);
        startActivity(intent);
    }
}