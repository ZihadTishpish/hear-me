package myshout.corsair.com.shout;

import android.app.Activity;
import android.os.Bundle;
import android.util.SparseArray;
import android.widget.ExpandableListView;
import android.widget.TextView;

/**
 * Created by Tiash on 2/22/2016.
 */
    public class PoliceMain extends Activity {
        // more efficient than HashMap for mapping integers to objects
        SparseArray<Group> groups = new SparseArray<Group>();

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.policemain);
            createData();
            ExpandableListView listView = (ExpandableListView) findViewById(R.id.listView);
            MyExpandableListAdapter adapter = new MyExpandableListAdapter(this,
                    groups);
            listView.setAdapter(adapter);
        }

        public void createData() {

       /* for (int j = 0; j < 5; j++) {
            Group group = new Group("Test " + j);
            for (int i = 0; i < 5; i++) {
                group.children.add("Sub Item" + i);
            }
            groups.append(j, group);
        }
        */

            Group group = new Group ("Dhaka");
            int i=5;
            group.children.add ("Ramna Thana - " + "0171337312" + i);
            //group.children.add ("0171337312" + i);
            i++;
            group.children.add ("Dhanmondi Thana - " + "0171337312" + i);
            //group.children.add ("Dhanmondi Thana");
            //group.children.add ("0171337312i");

            i++;
            group.children.add ("Shahbag Thana - " + "0171337312" + i);
            //group.children.add ("Shahbag Thana");
            //group.children.add ("0171337312i");
            i++;
            group.children.add ("New Market Thana - " + "0171337312" + i);
            //group.children.add ("New Market Thana");
            //group.children.add ("0171337312i");
            i++;
            group.children.add ("Lalabg Thana - " + "0171337312" + i);
            //group.children.add ("Lalbag Thana");
            //group.children.add ("0171337312i");
            i++;
            groups.append (0, group);
///////////////////////////////////////////////////////

            Group group1 = new Group ("Narayanganj");
            i = 7;
            group1.children.add ("Bandar Thana - " + "0171337314" + i);
            // group1.children.add ("Bandar Thana");
            //group1.children.add ("0171337334i");
            i++;
            group1.children.add ("Shiddhirgaon Thana - " + "0171337314" + i);
            //group1.children.add ("Shiddhirgaon Thana");
            //group1.children.add ("0171337334i");
            i++;
            group1.children.add ("Araihajar Thana - " + "0171337314" + i);
            //group1.children.add ("Araihajar Thana");
            //group1.children.add ("0171337334i");
            i++;
            group1.children.add ("Sonargaon Thana - " + "0171337314" + i);
            //group1.children.add ("Sonargaon Thana");
            //group1.children.add ("0171337334i");
            i++;

            groups.append (1, group1);

            Group group2 = new Group ("Gazipur");
            i = 5;
            group2.children.add ("Kaliyakair Thana - " + "0171337336" + i);
            //group2.children.add ("Kaliyakair Thana");
            //group2.children.add ("0171337336i");
            i++;
            group2.children.add ("Sripur Thana - " + "0171337336" + i);
            //group2.children.add ("Sripur Thana");
            //group2.children.add ("0171337336i");
            i++;
            group2.children.add ("Kapasia Thana - " + "0171337336" + i);

            //group2.children.add ("Kapasia Thana");
            //group2.children.add ("0171337336i");
            i++;
            group2.children.add ("Kaliganjr Thana - " + "0171337336" + i);

            //group2.children.add ("Kaliganj Thana");
            //group2.children.add ("0171337336i");
            i++;

            groups.append (2, group2);


        }

    }
