package myshout.corsair.com.shout;
import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class playAlarm extends Activity
{
    MediaPlayer tias;
    private long mLastClickTime = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.siren);


        final AudioManager mAudioManager = (AudioManager) getSystemService (AUDIO_SERVICE);
        final int originalVolume =  mAudioManager.getStreamVolume (AudioManager.STREAM_MUSIC);
        mAudioManager.setStreamVolume (AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume (AudioManager.STREAM_MUSIC), 0);

        tias = (MediaPlayer) MediaPlayer.create(this,R.raw.mary);
        //tias.setVolume(1, 1);
        tias.start();
        tias.setLooping(true);
        Button stop= (Button) findViewById(R.id.stop);

        stop.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                /// tias.stop();
                //  startActivity(new Intent(getApplicationContext(), MainActivity.class));
                tias.stop();
                (playAlarm.this).finish();
                Intent loginscreen = new Intent(getApplicationContext(), MainActivity.class);
                loginscreen.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(loginscreen);
            }
        });



    }
	/*
	@Override
	public void onBackPfressed()
	{

  	  	tias.stop();
			finish();
			 startActivity(new Intent(getApplicationContext(), MainActivity.class));


	}*/


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_POWER))
        {
            tias.stop();
        }

        else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN )
        {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 800)
            {
                //Toast.makeText(getApplicationContext(), "Detected", Toast.LENGTH_LONG).show();
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            // Toast.makeText(getApplicationContext(),"Tap Again to Exit", Toast.LENGTH_LONG).show();
        }

        else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP )
        {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 800)
            {
                //Toast.makeText(getApplicationContext(),"Detected too", Toast.LENGTH_LONG).show();
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            // Toast.makeText(getApplicationContext(),"Tap Again to Exit", Toast.LENGTH_LONG).show();
        }
        if (keyCode == KeyEvent.KEYCODE_BACK )
        { tias.stop();
            (playAlarm.this).finish();
            Intent loginscreen = new Intent(this, MainActivity.class);
            //loginscreen.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(loginscreen);
        }


        return true;
    }


}
