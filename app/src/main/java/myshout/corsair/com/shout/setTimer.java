package myshout.corsair.com.shout;

/**
 * Created by Tiash on 2/15/2016.
 */

        import java.util.Calendar;
        import java.util.Date;


        import android.app.Activity;
        import android.app.AlarmManager;
        import android.app.Notification;
        import android.app.NotificationManager;
        import android.app.PendingIntent;
        import android.content.BroadcastReceiver;
        import android.content.ContentResolver;
        import android.content.Context;
        import android.content.Intent;
        import android.content.IntentFilter;
        import android.content.SharedPreferences;
        import android.database.Cursor;
        import android.net.Uri;
        import android.os.Bundle;
        import android.provider.ContactsContract;
        import android.provider.ContactsContract.Contacts;
        import android.telephony.SmsManager;
        import android.view.View;
        import android.view.View.OnClickListener;
        import android.widget.AdapterView;
        import android.widget.AdapterView.OnItemClickListener;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageButton;
        import android.widget.TextView;
        import android.widget.TimePicker;
        import android.widget.Toast;

public class setTimer extends Activity

{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.setalarm);


        Button b = (Button) findViewById(R.id.button1);

        b.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
                Calendar c = Calendar.getInstance();
                Date date = c.getTime();
                int i = date.getHours();
                int j = date.getMinutes();

                TimePicker time = (TimePicker) findViewById(R.id.timePicker1);
                int t= time.getCurrentHour();
                int u=time.getCurrentMinute();
                timeDiffrence diff = new timeDiffrence(i,j,t,u);
	    	  /*TextView mytext = (TextView) findViewById(R.id.textView1);
	  		  mytext.setText("time Diffrence: "+diff.getTimeDiff());*/


                SharedPreferences smsSet = getSharedPreferences("smsCondition",Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = smsSet.edit();
                //edit.putBoolean("trigger", true);
                EditText e1 = (EditText) findViewById(R.id.num);
                EditText e2 = (EditText) findViewById(R.id.msg);

                //String number = makeValidNumber(e1.getText().toString());



                edit.putString("number",e1.getText().toString() );
                edit.putString("message",e2.getText().toString() );
                edit.commit();
                // SharedPreferences msgset = new SharedPreferences("myset");

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MINUTE, diff.getTimeDiff());
                //Create a new PendingIntent and add it to the AlarmManager
                Intent intent = new Intent(getApplicationContext(), AlarmReceiverActivity.class);
                Toast.makeText(getApplicationContext(), "Sending message after scheduled time"+diff.getTimeDiff()+" minutes", Toast.LENGTH_LONG).show();
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),pendingIntent);

                //PendingIntent.

            }

        });

        Button c = (Button) findViewById(R.id.button2);
        c.setOnClickListener (new OnClickListener()
        {
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), AlarmReceiverActivity.class);
                PendingIntent.getActivity(getApplicationContext(), 1, intent, PendingIntent.FLAG_UPDATE_CURRENT).cancel();

                Intent intent2 = new Intent(getApplicationContext(), MainActivity.class);
                PendingIntent pIntent = PendingIntent.getActivity(setTimer.this, 0, intent2, 0);

                // Build notification
                // Actions are just fake
                Notification noti = new Notification.Builder(setTimer.this)
                        .setContentTitle("Timer message Stopped ")
                        .setContentText("Press here to set a new Timers SMS").setSmallIcon(R.mipmap.ic_launcher)
                        .setContentIntent(pIntent).build();
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                // hide the notification after its selected
                noti.flags |= Notification.FLAG_AUTO_CANCEL;

                notificationManager.notify(0, noti);

            }

        });


        ImageButton d = (ImageButton) findViewById(R.id.imageButton1);
        d.setOnClickListener (new OnClickListener()
        {     @Override
              public void onClick(View v)
            {
                Intent intent= new Intent(Intent.ACTION_PICK,  Contacts.CONTENT_URI);
                startActivityForResult(intent, 1);
            }

        });
    }

   /* @Override
    public void onBackPressed()
    {
			/*finish();
			Intent intent = new Intent (Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);*/
        //tias.stop();
     /////////////////////////////////////////// upor er part baad
       // (setTimer.this).finish();

       /* Intent loginscreen = new Intent(this, MainActivity.class);
        loginscreen.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(loginscreen);


    }*/
    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data)
    {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode)
        {
            case (1) :
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();

                    ContentResolver cr = getContentResolver();
                    Cursor cur =  managedQuery(contactData, null, null, null, null);

                    if (cur.moveToFirst())
                    {
                        String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        // TODO Fetch other Contact details as you want to use


                        String id = cur.getString(
                                cur.getColumnIndex(ContactsContract.Contacts._ID));

                        if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
                        {
                            Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",  new String[]{id}, null);
                            while (pCur.moveToNext())
                            {

                                EditText e1 = (EditText) findViewById(R.id.num);
                                String number = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                e1.setText(number);

                            }
                            pCur.close();

                        }
                    }
                    break;
                }
        }


    }
}

